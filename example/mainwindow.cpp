#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QHBoxLayout>
#include <QPushButton>
#include <QVBoxLayout>
#include <QDialog>
#include <QLineEdit>

#include "generictabledelegator.h"
#include "generictablemodel.h"
#include "generictableview.h"

#include "propertyselectioncombobox.h"
#include "propertyselectiondoublespinbox.h"
#include "propertyselectionlineedit.h"
#include "propertyselectionspinbox.h"
#include "propertyselectioncheckbox.h"

namespace  {
    /*!
     * \brief The PropertyDialog class
	 * Simple dialog to select the representation and the name of the property
     */
    class PropertyDialog: public QDialog
    {
    public:
        PropertyDialog(QWidget *parent = nullptr): QDialog(parent) {
            QVBoxLayout* layout = new QVBoxLayout();

			cbRepresentationTypes = new QComboBox(this);
			cbRepresentationTypes->addItems({"Text", "Integer", "Double", "Selection", "Boolean"});
			layout->addWidget(cbRepresentationTypes);

            le = new QLineEdit("Property name", this);
            layout->addWidget(le);

            QPushButton* btn = new QPushButton("OK");
            layout->addWidget(btn);

            // capture a reference (&)
            connect(btn, &QPushButton::clicked, [&] { this->accept(); });

            this->setLayout(layout);
        };

        QString name() {return le->text();}
		QString representation() {return cbRepresentationTypes->currentText();}

    private:
        QLineEdit* le;
		QComboBox* cbRepresentationTypes;
    };
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
      , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QVBoxLayout *mainLayout = new QVBoxLayout();

    QHBoxLayout *toolButtons = new QHBoxLayout(); // for adding tool buttons like adding/removing properties
    QPushButton *add = new QPushButton("Add", this);
    toolButtons->addWidget(add);
    QPushButton *remove = new QPushButton("remove", this);
    toolButtons->addWidget(remove);
    QPushButton *removeAll = new QPushButton("removeAll", this);
    toolButtons->addWidget(removeAll);
    connect(add, &QPushButton::clicked, this, &MainWindow::addProperty);
    connect(remove, &QPushButton::clicked, this, &MainWindow::removeProperty);
    connect(removeAll, &QPushButton::clicked, this, &MainWindow::removeAll);

    mainLayout->addLayout(toolButtons);

    // Create new GenericTableView and the model.
    GenericTableView *view = new GenericTableView(this);
    model = new GenericTableModel(view);
	QStringList header = {"Name", "Value", "Representation", "Required"};
    model->setHeader(header);
    view->setModel(model);

    auto delegator = new GenericTableDelegator(view);
    view->setItemDelegate(delegator);
    mainLayout->addWidget(view);

    ui->centralwidget->setLayout(mainLayout);

    delegator->registerWrapperWidget("Text", QSharedPointer<PropertySelectionWrapper>(
                                  new PropertySelectionLineEdit(this)));
    delegator->registerWrapperWidget("Double", QSharedPointer<PropertySelectionWrapper>(
                                  new PropertySelectionDoubleSpinBox(this)));
	delegator->registerWrapperWidget("Boolean", QSharedPointer<PropertySelectionWrapper>(
										 new PropertySelectionCheckBox(this)));

    auto pSpinbox = QSharedPointer<PropertySelectionWrapper>(new PropertySelectionSpinBox(this));
    // Set initial value
    // Also ranges and whatever you want can be done here
    static_cast<QSpinBox *>(pSpinbox->widget())->setValue(10);
    delegator->registerWrapperWidget("Integer", pSpinbox);

    auto pCombo = QSharedPointer<PropertySelectionWrapper>(new PropertyselectionCombobox(this));
    // set items in the combobox
    QComboBox *cb = static_cast<QComboBox *>(pCombo->widget());
    cb->addItems({"Test1", "Test2"});
    delegator->registerWrapperWidget("Selection", pCombo);
}

MainWindow::~MainWindow()
{
    delete ui;
}

/*!
 * \brief MainWindow::addProperty
 * Opens a dialog to create a new property.
 * After the property was selected, the corresponding wrapper will be created. This wrapper is used to
 * modify the value afterwards
 */
void MainWindow::addProperty()
{
    PropertyDialog dialog(this);
    dialog.exec();
	// Get name and representation from the dialog
    QString name = dialog.name();
	QString representation = dialog.representation();

	Property p(name, representation);

	model->appendProperty(p);
}

void MainWindow::removeAll()
{
  while(model->removeProperty(0))
  {}
}

void MainWindow::removeProperty()
{
    model->removeProperty(0); // remove always the first property
}
