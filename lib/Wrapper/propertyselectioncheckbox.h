#ifndef PROPERTYSELECTIONCHECKBOX_H
#define PROPERTYSELECTIONCHECKBOX_H

#include "../property.h"

class QCheckBox;

class PropertySelectionCheckBox : public PropertySelectionWrapper
{
public:
	PropertySelectionCheckBox(QWidget *parent = nullptr);
	virtual bool setWidgetValue(const QVariant &value) override;
	virtual QVariant widgetValue() const override;
	virtual bool validValue(const QVariant &value) const override;
	virtual QVariant initialValue() const override;

private:
	QCheckBox* m_cb{nullptr};
};

#endif // PROPERTYSELECTIONCHECKBOX_H
