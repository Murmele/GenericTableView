#include "propertyselectioncheckbox.h"
#include <QCheckBox>

PropertySelectionCheckBox::PropertySelectionCheckBox(QWidget *parent):
	PropertySelectionWrapper(new QCheckBox(parent))
{
	m_cb = static_cast<QCheckBox*>(widget());
}

bool PropertySelectionCheckBox::setWidgetValue(const QVariant &value)
{
	bool ok = false;
	int val = value.toBool();
	m_cb->setChecked(val);
	return true;
}

QVariant PropertySelectionCheckBox::widgetValue() const
{
	return m_cb->isChecked();
}

bool PropertySelectionCheckBox::validValue(const QVariant &value) const
{
	//int v = value.toBool(); always valid
	return true;
}

QVariant PropertySelectionCheckBox::initialValue() const
{
	return false;
}
