#include "generictabledelegator.h"

#include "generictablemodel.h"
#include "property.h"

GenericTableDelegator::GenericTableDelegator(QObject *parent) : QStyledItemDelegate(parent) {}

QWidget *GenericTableDelegator::createEditor(QWidget *parent,
                                            const QStyleOptionViewItem &option,
                                            const QModelIndex &index) const
{
    Q_UNUSED(option)
    if (index.column() == GenericTableModel::Columns::Description)
        return nullptr;
    else if (index.column() == GenericTableModel::Columns::Value) {

		QString representation = index.data(GenericTableModel::RepresentationTypeRole).toString();
		auto wrapper = wrapperWidget(representation);
        //Only possible when widget gets not destroyed in destroy Editor
        if (wrapper) { // the widget it self is not a smartpointer, but the wrapper
            QWidget *w = wrapper->widget();
            w->setParent(parent);
            return w;
        }
    }

    return nullptr;
}

void GenericTableDelegator::destroyEditor(QWidget *editor, const QModelIndex &index) const
{
    Q_UNUSED(editor)
    Q_UNUSED(index)
    // Do not destory the editor, because it is stored in the Property.
    // By default createEditor() returns a new object and GenericTableDelegator takes over and
    // at the end of editing, the editor gets deleted. We wanna reuse the editor and therefore
    // it should not be deleted.
}

// model --> editor
void GenericTableDelegator::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    Q_UNUSED(editor)
    if (!index.isValid())
        return;

    QVariant val = index.model()->data(index, Qt::EditRole);
	auto wrapper = wrapperWidget(index.data(GenericTableModel::RepresentationTypeRole).toString());
    if (wrapper) // because the widget might already be deleted outside
        wrapper->setWidgetValue(val);
}

// editor --> model
void GenericTableDelegator::setModelData(QWidget *editor,
                                         QAbstractItemModel *model,
                                         const QModelIndex &index) const
{
    Q_UNUSED(editor)

    if (!index.isValid())
        return;

	auto wrapper = wrapperWidget(index.data(GenericTableModel::RepresentationTypeRole).toString());
    if (!wrapper)
        return;

    QVariant value = wrapper->widgetValue();
    model->setData(index, value, Qt::EditRole);
}

bool GenericTableDelegator::registerWrapperWidget(const QString& wrappername, QSharedPointer<PropertySelectionWrapper> wrapper)
{
    if (mWrappers.contains(wrappername))
        return false;

    mWrappers[wrappername] = wrapper;
    return true;
}

QSharedPointer<PropertySelectionWrapper> GenericTableDelegator::wrapperWidget(const QString& wrappername) const
{
    if (!mWrappers.contains(wrappername))
        return nullptr;

    return mWrappers[wrappername];
}

void GenericTableDelegator::removeWrapperWidget(const QString& wrappername)
{
    if (!mWrappers.contains(wrappername))
        return;

    mWrappers.take(wrappername); // remove pointer from hash
}

void GenericTableDelegator::updateEditorGeometry(QWidget *editor,
                                                 const QStyleOptionViewItem &option,
                                                 const QModelIndex &index) const
{
    if (!index.isValid())
        return;

    editor->setGeometry(option.rect);
}
