#include "property.h"

Property::Property() {}

Property::Property(const QString &name, const QString &representation)
	: m_name(name), m_representation(representation)
{}

Property::Property(const Property& p)
{
	operator=(p);
}

void Property::operator=(const Property& other) {
	m_name = other.m_name;
	m_representation = other.m_representation;
	m_required = other.m_required;
	m_value = other.m_value;
}
